"use strict";
// Questão 6 //
//Variavel endereco do tipo da interface criada anteriormente.//
var endereco;
//Atribuindo um endereço, uma rua, numero, bairro e cidade à variavel criada.//
endereco = {
    rua: 'Rua Lais Neto dos Reis',
    numero: 276,
    bairro: "Vila Julieta",
    cidade: "Resende"
};
//Utilize o console.log para exibir o conteúdo da variável endereço.//
console.log("Endereço Registrado:", endereco);
