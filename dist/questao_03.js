"use strict";
// Questão 3 //
//Crie uma classe de Funcionários com os atributos nome, salario hora, dias
// trabalhados, total de faltas no ano.
var Funcionarios = /** @class */ (function () {
    function Funcionarios(_nome, _salarioHora, _diasTrabalhados, _faltas) {
        this._nome = _nome;
        this._salarioHora = _salarioHora;
        this._diasTrabalhados = _diasTrabalhados;
        this._faltas = _faltas;
    }
    Object.defineProperty(Funcionarios.prototype, "nome", {
        //Criando os getters//
        get: function () {
            return this._nome;
        },
        //Criando os setters//
        set: function (nome) {
            if (nome == '') {
                throw new Error("Erro! Nome invalido");
            }
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "salarioHora", {
        get: function () {
            return this._salarioHora;
        },
        set: function (salarioHora) {
            if (salarioHora <= 0) {
                throw new Error("Erro! Salario hora invalido");
            }
            this._salarioHora = salarioHora;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "diasTrabalhados", {
        get: function () {
            return this._diasTrabalhados;
        },
        set: function (diasTrabalhados) {
            if (diasTrabalhados <= 0) {
                throw new Error("Erro! Dias trabalhados invalido");
            }
            this._diasTrabalhados = diasTrabalhados;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "faltas", {
        get: function () {
            return this._faltas;
        },
        set: function (faltas) {
            if (faltas <= 0) {
                throw new Error("Erro! Faltas invalida");
            }
            this._faltas = faltas;
        },
        enumerable: false,
        configurable: true
    });
    // Crie um método para calcular o salário bruto que será a multiplicação do salario
    // hora pelos dias trabalhados.
    Funcionarios.prototype.salarioBruto = function () {
        return this.salarioHora * this.diasTrabalhados;
    };
    // Crie um método para calcular a PLR. Se o numero de faltas for igual a zero a PLR
    // será igual ao salário bruto. Se o número de faltas for igual a 1 a PLR será 94% do
    // salário bruto. Se o número de faltas for igual a 2 a PLR será 92% do salário bruto.
    // Se o número de faltas for igual a 3 a PLR será 90% do salário bruto. se o número de
    // faltas for igual a 4 a PLR será 88% do salário bruto. Se o número de faltas for maior
    // ou igual a 5 a PLR será zero.
    Funcionarios.prototype.PLR = function () {
        var plr = 0;
        if (this.faltas <= 0) {
            plr = this.salarioBruto();
        }
        else if (this.faltas == 1) {
            plr = this.salarioBruto() * 0.94;
        }
        else if (this.faltas == 2) {
            plr = this.salarioBruto() * 0.92;
        }
        else if (this.faltas == 3) {
            plr = this.salarioBruto() * 0.90;
        }
        else if (this.faltas == 4) {
            plr = this.salarioBruto() * 0.88;
        }
        else if (this.faltas >= 5) {
            plr = 0;
        }
        return plr;
    };
    //Crie um método para calcular o desconto que será 5% do salário bruto.
    Funcionarios.prototype.desconto5 = function () {
        return this.salarioBruto() * 0.05;
    };
    //Crie um método para calcular o salário liquido. Salario bruto - desconto + PLR.
    Funcionarios.prototype.salarioLiquido = function () {
        var salarioDesconto = this.salarioBruto() - this.desconto5();
        return salarioDesconto + this.PLR();
        // return this.PLR() - this.desconto5() ;
    };
    return Funcionarios;
}());
// Imprima as seguintes mensagens
// O funcionário de nome xx tem o salário bruto de 999, teve 99 falta(s) e sua PLR foi
// de 999
// O funcionário de nome xx tem o salário bruto de 999, o desconto de 999, a PLR de
// 999 e o salário líquido de 9999
var func1 = new Funcionarios('Déborah', 20, 200, 0);
console.log("O funcion\u00E1rio " + func1.nome + " tem o sal\u00E1rio bruto de " + func1.salarioBruto() + ", \n    teve " + func1.faltas + " faltas e sua PLR foi de " + func1.PLR() + ".");
console.log("O funcion\u00E1rio " + func1.nome + " tem o sal\u00E1rio bruto de " + func1.salarioBruto() + ", \n        o desconto de " + func1.desconto5() + ", a PLR de " + func1.PLR() + " e o sal\u00E1rio l\u00EDquido de " + func1.salarioLiquido() + ".");
// try {
//     func1.nome = ''
//     console.log(func1.nome)
// } catch (err: any) {
//     console.log(err.message)
// }
