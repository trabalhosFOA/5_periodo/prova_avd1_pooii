"use strict";
// 2) Escreva a classe ConversaoDeUnidadesDeTempo com métodos estáticos para
// conversão
// das unidades de velocidade segundo a lista abaixo, por exemplo
// converMinutoParaSegundo. O método deverá receber a quantidade de minutos e
// retornar o valor em segundos.
// • 1 minuto = 60 segundos
// • 1 hora = 60 minutos
// • 1 dia = 24 horas
// • 1 semana = 7 dias
// • 1 mês = 30 dias
// • 1 ano = 365.25 dias
var ConversaoDeUnidadesDeTempo = /** @class */ (function () {
    function ConversaoDeUnidadesDeTempo() {
    }
    ConversaoDeUnidadesDeTempo.converMinutoParaSegundo = function (minutos) {
        var segundos = minutos * 60;
        return segundos;
    };
    ConversaoDeUnidadesDeTempo.converHoraParaMinuto = function (horas) {
        var minutos = horas * 60;
        return minutos;
    };
    ConversaoDeUnidadesDeTempo.converDiaParaHora = function (dias) {
        var horas = dias * 24;
        return horas;
    };
    ConversaoDeUnidadesDeTempo.converSemanaParaDias = function (semanas) {
        var dias = semanas * 7;
        return dias;
    };
    ConversaoDeUnidadesDeTempo.converMesParaDias = function (meses) {
        var dias = meses * 30;
        return dias;
    };
    ConversaoDeUnidadesDeTempo.converAnoParaDias = function (anos) {
        var dias = anos * 365;
        return dias;
    };
    return ConversaoDeUnidadesDeTempo;
}());
var ex1 = ConversaoDeUnidadesDeTempo.converMinutoParaSegundo(2);
var ex2 = ConversaoDeUnidadesDeTempo.converHoraParaMinuto(12);
var ex3 = ConversaoDeUnidadesDeTempo.converDiaParaHora(2);
var ex4 = ConversaoDeUnidadesDeTempo.converSemanaParaDias(10);
var ex5 = ConversaoDeUnidadesDeTempo.converMesParaDias(12);
var ex6 = ConversaoDeUnidadesDeTempo.converAnoParaDias(2);
console.log('Minuto para Segundo: ', ex1);
console.log('Hora para Minuto: ', ex2);
console.log('Dia para Hora: ', ex3);
console.log('Semana para Dia: ', ex4);
console.log('Mes para Dia: ', ex5);
console.log('Ano para Dia: ', ex6);
