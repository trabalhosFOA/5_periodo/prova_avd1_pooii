"use strict";
// Questão 2 //
//Criando uma classe de Veiculos contendo os atributos modelo, marca, ano//
//valorLocacao e quantidade de dias//
var Veiculos = /** @class */ (function () {
    //Criando atributos privados que só podem ser acessados pela propria classe//
    function Veiculos(_modelo, _marca, _ano, _valorLocacao, _quantidadeDias) {
        this._modelo = _modelo;
        this._marca = _marca;
        this._ano = _ano;
        this._valorLocacao = _valorLocacao;
        this._quantidadeDias = _quantidadeDias;
    }
    Object.defineProperty(Veiculos.prototype, "modelo", {
        //Criando os getters//
        get: function () {
            return this._modelo;
        },
        //Criando os setters//
        //Quando a definição de modelo estiver em branco retorna uma mensagem inválida.//
        set: function (modelo) {
            if (modelo == '') {
                throw new Error("Modelo inaceitavel");
            }
            this._modelo = modelo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Veiculos.prototype, "marca", {
        get: function () {
            return this._marca;
        },
        //Quando a definição de marca estiver em branco retorna uma mensagem inválida.//
        set: function (marca) {
            if (marca == '') {
                throw new Error("Marca inaceitavel");
            }
            this._marca = marca;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Veiculos.prototype, "ano", {
        get: function () {
            return this._ano;
        },
        //Quando a definição de ano for menor ou igual a zero retorna uma mensagem inválida.//
        set: function (ano) {
            if (ano <= 0) {
                throw new Error("Ano inaceitavel");
            }
            this._ano = ano;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Veiculos.prototype, "valorLocacao", {
        get: function () {
            return this._valorLocacao;
        },
        //Quando a definição de valorLocacao for menor ou igual a zero retorna uma mensagem inválida.//
        set: function (valorLocacao) {
            if (valorLocacao <= 0) {
                throw new Error("Valor da Locacao inaceitavel");
            }
            this._valorLocacao = valorLocacao;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Veiculos.prototype, "quantidadeDias", {
        get: function () {
            return this._quantidadeDias;
        },
        //Quando a definição de quantidadeDias for menor ou igual a zero retorna uma mensagem inválida.//
        set: function (quantidadeDias) {
            if (quantidadeDias <= 0) {
                throw new Error("Quantidade de Dias inaceitavel");
            }
            this._quantidadeDias = quantidadeDias;
        },
        enumerable: false,
        configurable: true
    });
    //Metodo que recebe a quantidade de dias e valor da locação. //
    //Ele retorna o resultado do calculo da multiplicação da quantidade de dias pelo valor da locação //
    Veiculos.prototype.passeio = function () {
        var valor = this.quantidadeDias * this.valorLocacao;
        return valor;
    };
    return Veiculos;
}());
//Criando um objeto carro a partir do modelo Veiculos//
var carro = new Veiculos('Meriva', 'Chevrolet', 2012, 70, 5);
//Mostrando as informações do carro//
console.log(carro);
console.log("Quantidade de dias: " + carro.quantidadeDias + "\nValor da loca\u00E7\u00E3o: " + carro.valorLocacao + "\nResultado: " + carro.passeio() + "\n\n");
//Exemplos//
// try{
//     carro.marca = ''
// } catch (err:any){
//     console.log(err.message)
// }
// try{
//     carro.modelo = ''
// } catch (err:any){
//     console.log(err.message)
// }
// try{
//     carro.ano = 0
// } catch (err:any){
//     console.log(err.message)
// }
// try{
//     carro.quantidadeDias = 0
//     carro.valorLocacao = 0
// } catch (err:any){
//     console.log(err.message);
// }
//console.log(carro)
//console.log(`Quantidade de dias: ${carro.quantidadeDias}\nValor da locação: ${carro.valorLocacao}\nResultado: ${carro.passeio()}`);
