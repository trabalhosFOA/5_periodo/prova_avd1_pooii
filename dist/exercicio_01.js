"use strict";
// 2) Escreva a classe ConversaoDeUnidadesDeTempo com métodos estáticos para
// conversão
// das unidades de velocidade segundo a lista abaixo, por exemplo
// converMinutoParaSegundo. O método deverá receber a quantidade de minutos e
// retornar o valor em segundos.
// • 1 minuto = 60 segundos
// • 1 hora = 60 minutos
// • 1 dia = 24 horas
// • 1 semana = 7 dias
// • 1 mês = 30 dias
// • 1 ano = 365.25 dias
var ConversaoDeUnidadesDeTempo = /** @class */ (function () {
    function ConversaoDeUnidadesDeTempo() {
    }
    ConversaoDeUnidadesDeTempo.converMinutoParaSegundo = function (minutos) {
        var segundos = minutos * 60;
        return segundos;
    };
    ConversaoDeUnidadesDeTempo.converHoraParaMinuto = function (_a) {
        var horas = _a.horas;
        var minutos = horas * 60;
        return minutos;
    };
    ConversaoDeUnidadesDeTempo.converDiaParaHora = function (_a) {
        var dias = _a.dias;
        var horas = dias * 24;
        return horas;
    };
    ConversaoDeUnidadesDeTempo.converSemanaParaDias = function (_a) {
        var semanas = _a.semanas;
        var dias = semanas / 7;
        return dias;
    };
    ConversaoDeUnidadesDeTempo.converMesParaDias = function (_a) {
        var meses = _a.meses;
        var dias = meses * 30;
        return dias;
    };
    ConversaoDeUnidadesDeTempo.converAnoParaDias = function (_a) {
        var anos = _a.anos;
        var dias = anos * 365;
        return dias;
    };
    return ConversaoDeUnidadesDeTempo;
}());
var c1 = ConversaoDeUnidadesDeTempo.converMinutoParaSegundo(2);
console.log(c1);
