// 2) Escreva a classe ConversaoDeUnidadesDeTempo com métodos estáticos para
// conversão
// das unidades de velocidade segundo a lista abaixo, por exemplo
// converMinutoParaSegundo. O método deverá receber a quantidade de minutos e
// retornar o valor em segundos.
// • 1 minuto = 60 segundos
// • 1 hora = 60 minutos
// • 1 dia = 24 horas
// • 1 semana = 7 dias
// • 1 mês = 30 dias
// • 1 ano = 365.25 dias

class ConversaoDeUnidadesDeTempo {
    static converMinutoParaSegundo(minutos:number) {
        let segundos = minutos * 60
        return segundos;
    }

    static converHoraParaMinuto(horas:number) {
        let minutos = horas * 60
        return minutos;
    }

    static converDiaParaHora(dias:number) {
        let horas = dias * 24
        return horas;
    }

    static converSemanaParaDias(semanas:number) {
        let dias = semanas * 7
        return dias;
    }

    static converMesParaDias(meses:number) {
        let dias = meses * 30
        return dias;
    }

    static converAnoParaDias(anos:number) {
        let dias = anos * 365
        return dias;
    }

}

let ex1 = ConversaoDeUnidadesDeTempo.converMinutoParaSegundo(2);
let ex2 = ConversaoDeUnidadesDeTempo.converHoraParaMinuto(12);
let ex3 = ConversaoDeUnidadesDeTempo.converDiaParaHora(2);
let ex4 = ConversaoDeUnidadesDeTempo.converSemanaParaDias(10);
let ex5 = ConversaoDeUnidadesDeTempo.converMesParaDias(12);
let ex6 = ConversaoDeUnidadesDeTempo.converAnoParaDias(2);

console.log('Minuto para Segundo: ', ex1);
console.log('Hora para Minuto: ', ex2);
console.log('Dia para Hora: ', ex3);
console.log('Semana para Dia: ', ex4);
console.log('Mes para Dia: ', ex5);
console.log('Ano para Dia: ', ex6);
