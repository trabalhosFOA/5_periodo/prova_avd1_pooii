// Questão 3 //

//Crie uma classe de Funcionários com os atributos nome, salario hora, dias
// trabalhados, total de faltas no ano.
class Funcionarios {
    constructor(
        private _nome: string,
        private _salarioHora: number,
        private _diasTrabalhados: number,
        private _faltas: number
    ) { }

    //Criando os getters//
    get nome() {
        return this._nome
    }

    get salarioHora() {
        return this._salarioHora
    }

    get diasTrabalhados() {
        return this._diasTrabalhados
    }

    get faltas() {
        return this._faltas
    }

    //Criando os setters//
    set nome(nome: string) {
        if (nome == '') {
            throw new Error("Erro! Nome invalido")
        }
        this._nome = nome
    }

    set salarioHora(salarioHora: number) {
        if (salarioHora <= 0) {
            throw new Error("Erro! Salario hora invalido")
        }
        this._salarioHora = salarioHora
    }

    set diasTrabalhados(diasTrabalhados: number) {
        if (diasTrabalhados <= 0) {
            throw new Error("Erro! Dias trabalhados invalido")
        }
        this._diasTrabalhados = diasTrabalhados
    }

    set faltas(faltas: number) {
        if (faltas <= 0) {
            throw new Error("Erro! Faltas invalida")
        }
        this._faltas = faltas
    }

    // Crie um método para calcular o salário bruto que será a multiplicação do salario
    // hora pelos dias trabalhados.
    salarioBruto() {
        return this.salarioHora * this.diasTrabalhados
    }

    // Crie um método para calcular a PLR. Se o numero de faltas for igual a zero a PLR
    // será igual ao salário bruto. Se o número de faltas for igual a 1 a PLR será 94% do
    // salário bruto. Se o número de faltas for igual a 2 a PLR será 92% do salário bruto.
    // Se o número de faltas for igual a 3 a PLR será 90% do salário bruto. se o número de
    // faltas for igual a 4 a PLR será 88% do salário bruto. Se o número de faltas for maior
    // ou igual a 5 a PLR será zero.

    PLR() {
        let plr = 0;
        if (this.faltas <= 0) {
            plr = this.salarioBruto()
        } else if (this.faltas == 1) {
            plr = this.salarioBruto() * 0.94
        } else if (this.faltas == 2) {
            plr = this.salarioBruto() * 0.92
        } else if (this.faltas == 3) {
            plr = this.salarioBruto() * 0.90
        } else if (this.faltas == 4) {
            plr = this.salarioBruto() * 0.88
        } else if (this.faltas >= 5) {
            plr = 0
        }

        return plr;
    }

    //Crie um método para calcular o desconto que será 5% do salário bruto.

    desconto5() {
        return this.salarioBruto() * 0.05
    }

    //Crie um método para calcular o salário liquido. Salario bruto - desconto + PLR.
    salarioLiquido() {
        let salarioDesconto = this.salarioBruto() - this.desconto5()
        return salarioDesconto + this.PLR();
        // return this.PLR() - this.desconto5() ;
    }

}


// Imprima as seguintes mensagens
// O funcionário de nome xx tem o salário bruto de 999, teve 99 falta(s) e sua PLR foi
// de 999
// O funcionário de nome xx tem o salário bruto de 999, o desconto de 999, a PLR de
// 999 e o salário líquido de 9999


const func1 = new Funcionarios('Déborah', 20, 200, 0);

console.log(
    `O funcionário ${func1.nome} tem o salário bruto de ${func1.salarioBruto()}, 
    teve ${func1.faltas} faltas e sua PLR foi de ${func1.PLR()}.`
)

console.log(
    `O funcionário ${func1.nome} tem o salário bruto de ${func1.salarioBruto()}, 
        o desconto de ${func1.desconto5()}, a PLR de ${func1.PLR()} e o salário líquido de ${func1.salarioLiquido()}.`
)


// try {
//     func1.nome = ''
//     console.log(func1.nome)
// } catch (err: any) {
//     console.log(err.message)
// }