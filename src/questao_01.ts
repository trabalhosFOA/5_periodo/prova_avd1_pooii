// 1) Crie uma classe com os atributos produto, preço, quantidade), Crie os métodos
// getters e setters. Crie validação para não aceitar nome em branco, preco e
// quantidade com valor 0 (zero). Considerando que são oferecidos descontos pelo
// número de quantidade comprada, seguindo a tabela abaixo:
// a. Até 10 unidades: não tem desconto
// b. de 11 a 20 unidades: 10% de desconto
// c. de 21 a 50 unidades: 20% de desconto
// d. acima de 50 unidades: 25% de desconto
// Imprima a seguinte mensagem: O produto xxx de preço 999, quantidade comprada
// de 999 teve o desconto de 999 e o valor pago foi de 99999

class Produtos {
    constructor (
        private _produto: string,
        private _preco: number,
        private _quantidade: number
        ){}

//Criando os getters//
        get produto(){
            return this._produto;
        }

        get preco(){
            return this._preco;
        }

        get quantidade(){
            return this._quantidade;
        }

//Criando os setters//

        set produto(produto: string){
            if(produto == ''){
                throw new Error("Erro! Nome do produto não pode ser em branco.")
            }
            this._produto = produto
        }

        set preco(preco: number){
            if(preco <= 0){
                throw new Error("Erro! Preço do produto não pode ser menor ou igual a ZERO.")
            }
            this._preco = preco
        }

        set quantidade(quantidade: number){
            if(quantidade <= 0){
                throw new Error("Erro! Quantidade do produto não pode ser menor ou igual a ZERO.")
            }
            this._quantidade = quantidade
        }

        descontoProduto(){
            var valorTotal = this.quantidade * this.preco
            var valorFinal = 0;
            if(this.quantidade <= 10)
            {
                valorFinal = valorTotal;
            }else if(this.quantidade > 10 && this.quantidade <= 20)
            {
                valorFinal = valorTotal * 0.10;
            }else if(this.quantidade > 20 && this.quantidade <= 50)
            {
                valorFinal = valorTotal * 0.20;
            }else if(this.quantidade > 50)
            {
                valorFinal = valorTotal * 0.25;
            }

            return valorFinal;
        }

        valorProduto(){
            var precoTotal = this.quantidade * this.preco
            return precoTotal - this.descontoProduto();
        }

    }

const v1 = new Produtos('Batata', 10, 30)


console.log(`
    O produto ${v1.produto} com preço ${v1.preco},00 teve a quantidade comprada de ${v1.quantidade}. 
    Com base na quantidade comprada obteve um desconto de ${v1.descontoProduto()},00. 
    Seu valor pago foi de ${v1.valorProduto()},00`
    );

console.log(v1)

// try {
//     v1.produto = ''
//     console.log(v1.produto)
// } catch (err:any) {
//     console.log(err.message)
// }

// try {
//     v1.preco = 0
//     console.log(v1.preco)
// } catch (err:any) {
//     console.log(err.message)
// }

// try {
//     v1.quantidade = 0
//     console.log(v1.quantidade)
// } catch (err:any) {
//     console.log(err.message)
// }
